import { ADD_TO_CART, CHANGE_DETAIL } from "../../constant/shoeConstant";
import { dataShoe } from "../../dataShoe";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DETAIL:
      {
        state.detail = action.payload;
      }
      return { ...state };
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};

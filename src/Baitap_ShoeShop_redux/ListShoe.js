import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return <ItemShoe data={item} />;
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
// {this.renderListShoe()}
let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);

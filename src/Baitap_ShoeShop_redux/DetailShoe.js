import React, { Component } from "react";
import { connect } from "react-redux";
class DetailShoe extends Component {
  render() {
    return (
      <div className="row">
        <img src={this.props.detail.image} className="col-4" alt="" />
        <div className="col-8 d-flex align-items-center">
          <div>
            <h3>{this.props.detail.name} </h3>
            <h5 className="font-italic text-danger">${this.props.detail.price} </h5>
            <p>{this.props.detail.description} </p>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);

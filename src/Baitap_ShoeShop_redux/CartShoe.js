import React, { Component } from "react";
import { connect } from "react-redux";

class CartShoe extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td> {item.id} </td>
          <td> {item.name} </td>
          <td> {item.price * item.number} </td>
          <td>
            <button>-</button>
            {item.number}
            <button>+</button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};
export default connect(mapStateToProps)(CartShoe);

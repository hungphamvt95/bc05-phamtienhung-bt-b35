import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShop from "./Baitap_ShoeShop_redux/Ex_ShoeShop";

function App() {
  return (
    <div className="App">
      <Ex_ShoeShop />
    </div>
  );
}

export default App;
